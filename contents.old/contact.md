#Contact

* Website [http://kstar.gitlabpages.inria.fr/](http://kstar.gitlabpages.inria.fr)
* KSTAR OpenMP compiler download [here](download.md)
* Mailing-List [kstar-public@lists.gforge.inria.fr](mailto://kstar-public@lists.gforge.inria.fr)
