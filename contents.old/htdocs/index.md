#Welcome to KSTAR

##Introduction

The KSTAR project supports the development of Klang, a source-to-source compiler that
turns C programs with OpenMP pragmas to C programs with calls to either the StarPU or
the Kaapi runtime system.

You can see the supported features for both runtimes [here](features.md).
