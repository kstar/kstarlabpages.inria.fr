#Authors

Klang has been written by the members of the KSTAR project, namely :

* Olivier Aumage
* François Broquedis
* Pierrick Brunet
* Nathalie Furmento
* Thierry Gautier
* Samuel Thibault
* Philippe Virouleau

As the project is not yet public, there is no public mailing list yet.
