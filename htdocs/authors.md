#Authors

The KSTAR compiler has been written by the members of the project, namely :

* Olivier Aumage
* François Broquedis
* Pierrick Brunet
* Nathalie Furmento
* Thierry Gautier
* Samuel Pitoiset
* Samuel Thibault
* Philippe Virouleau

