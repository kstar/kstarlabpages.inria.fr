#Welcome to the Inria KSTAR Action website

##Introduction

The Inria KSTAR Action hosts the development of the KSTAR C/C++ OpenMP compiler and the Kastors benchmark suite.

##KSTAR
KSTAR (also nicknamed 'Klang-Omp') is a C and C++ source-to-source OpenMP compiler based on LLVM framework and its C/C++ front-end CLang. It translates OpenMP directives into calls to task-based runtime system APIs. KSTAR currently targets the StarPU runtime and optionnally the Kaapi runtime as well.
The compiler supports independent tasks as defined by the OpenMP specification revision 3.1 as well as dependent tasks introduced with OpenMP 4.
The two runtime systems currently targeted by KSTAR, [StarPU](http://starpu.gitlabpages.inria.fr/) and [Kaapi](http://kaapi.gforge.inria.fr), both natively implement dependent tasks. KSTAR also supports long established OpenMP constructs such as parallel loops and sections.


###Download
The KSTAR OpenMP compiler is available for download [here](download.md)


##Kastors Benchmarks
The KSTAR project also supports the development of the [KASTORS benchmark suite](https://gitlab.inria.fr/kstar/kastors). The KASTORS suite is a series of popular computing kernels that we have been ported on the OpenMP 4 depend task model, to explore the performance of compiler/runtime system pairs.

###Download
The KASTORS Benchmark Suite is is available for download [here](https://gitlab.inria.fr/kstar/kastors)
