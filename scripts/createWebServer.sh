#!/bin/bash

rm -rf public
mkdir public
rsync -az --chmod=ug=rwX,o=rX --exclude .htaccess htdocs/ public
